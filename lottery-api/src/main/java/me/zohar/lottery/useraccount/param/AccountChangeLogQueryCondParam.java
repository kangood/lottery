package me.zohar.lottery.useraccount.param;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.zohar.lottery.common.param.PageParam;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author sunflower
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AccountChangeLogQueryCondParam extends PageParam {

	private static final long serialVersionUID = 2101197901941600892L;
	/**
	 * 游戏代码
	 */
	private String gameCode;

	/**
	 * 开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startTime;

	/**
	 * 结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endTime;

	/**
	 * 账变类型代码
	 */
	private String accountChangeTypeCode;

	/**
	 * 用户账号id
	 */
	private String userAccountId;

}
