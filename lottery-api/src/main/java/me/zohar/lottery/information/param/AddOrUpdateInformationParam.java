package me.zohar.lottery.information.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import me.zohar.lottery.common.utils.IdUtils;
import me.zohar.lottery.information.domain.LotteryInformation;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * @author sunflower
 */
@Data
public class AddOrUpdateInformationParam {

	private String id;

	private String title;

	private String content;

	private String source;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
	private Date publishTime;
	
	public LotteryInformation convertToPo() {
		LotteryInformation po = new LotteryInformation();
		BeanUtils.copyProperties(this, po);
		po.setId(IdUtils.getId());
		return po;
	}

}
