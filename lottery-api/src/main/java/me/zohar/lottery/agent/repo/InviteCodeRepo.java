package me.zohar.lottery.agent.repo;

import me.zohar.lottery.agent.domain.InviteCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;

/**
 * @author sunflower
 */
public interface InviteCodeRepo extends JpaRepository<InviteCode, String>, JpaSpecificationExecutor<InviteCode> {
	
	InviteCode findTopByInviterIdOrderByPeriodOfValidityDesc(String inviterId);
	
	InviteCode findTopByCodeAndPeriodOfValidityGreaterThanEqual(String code, Date periodOfValidity);
	

}
