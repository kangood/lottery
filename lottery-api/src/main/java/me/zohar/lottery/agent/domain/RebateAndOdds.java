package me.zohar.lottery.agent.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.near.toolkit.model.ToString;

import javax.persistence.*;
import java.util.Date;

/**
 * 返点/赔率
 * 
 * @author zohar
 * @date 2019年6月2日
 *
 */
@Getter
@Setter
@Entity
@Table(name = "rebate_and_odds")
@DynamicInsert(true)
@DynamicUpdate(true)
public class RebateAndOdds extends ToString {

	private static final long serialVersionUID = 5599204851664644245L;
	/**
	 * 主键id
	 */
	@Id
	@Column(name = "id", length = 32)
	private String id;

	/**
	 * 返点
	 */
	private Double rebate;

	/**
	 * 赔率
	 */
	private Double odds;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 乐观锁版本号
	 */
	@Version
	private Long version;

}
