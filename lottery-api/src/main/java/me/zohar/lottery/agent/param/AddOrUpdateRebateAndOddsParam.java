package me.zohar.lottery.agent.param;

import lombok.Data;
import me.zohar.lottery.agent.domain.RebateAndOdds;
import me.zohar.lottery.common.utils.IdUtils;
import org.near.toolkit.model.ToString;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author sunflower
 */
@Data
public class AddOrUpdateRebateAndOddsParam extends ToString {

	private static final long serialVersionUID = 3973554244009233762L;
	private String id;

	/**
	 * 返点
	 */
	@NotNull
	@DecimalMin(value = "0", inclusive = true)
	private Double rebate;

	/**
	 * 赔率
	 */
	@NotNull
	@DecimalMin(value = "0", inclusive = true)
	private Double odds;

	public RebateAndOdds convertToPo(Date createTime) {
		RebateAndOdds po = new RebateAndOdds();
		BeanUtils.copyProperties(this, po);
		po.setId(IdUtils.getId());
		po.setCreateTime(createTime);
		return po;
	}

}
