package me.zohar.lottery.betting.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import me.zohar.lottery.useraccount.domain.UserAccount;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.near.toolkit.model.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * 追号情况
 *
 * @author zohar
 * @date 2019年5月15日
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "v_tracking_number_situation")
public class TrackingNumberSituation extends ToString {

    private static final long serialVersionUID = 5387856946662323027L;
    /**
     * 主键id
     */
    @Id
    @Column(name = "id", length = 32)
    private String id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 追号时间
     */
    private Date trackingNumberTime;

    /**
     * 游戏代码
     */
    private String gameCode;

    /**
     * 开始期号
     */
    private Long startIssueNum;

    /**
     * 投注底数金额
     */
    private Double baseAmount;

    /**
     * 追号即停
     */
    private Boolean winToStop;

    /**
     * 追号期数
     */
    private Long totalIssueCount;

    /**
     * 总投注金额
     */
    private Double totalBettingAmount;

    /**
     * 状态
     */
    private String state;

    /**
     * 完成期数
     */
    private Integer completedIssueCount;

    /**
     * 未完成的期数(状态为未开奖且未到投注截至时间)
     */
    private Integer uncompletedIssueCount;

    /**
     * 投注人用户账号id
     */
    @Column(name = "user_account_id", length = 32)
    private String userAccountId;

    /**
     * 投注人用户账号
     */
    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_account_id", updatable = false, insertable = false, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private UserAccount userAccount;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "tracking_number_order_id", foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    @OrderBy("issueNum ASC")
    private Set<TrackingNumberPlan> trackingNumberPlans;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "tracking_number_order_id", foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Set<TrackingNumberContent> trackingNumberContents;

}
