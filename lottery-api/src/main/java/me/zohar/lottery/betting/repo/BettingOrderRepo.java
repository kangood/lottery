package me.zohar.lottery.betting.repo;

import me.zohar.lottery.betting.domain.BettingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

public interface BettingOrderRepo extends JpaRepository<BettingOrder, String>, JpaSpecificationExecutor<BettingOrder> {

    /**
     * 根据游戏代码,期号,状态获取投注订单
     *
     * @param gameCode
     * @param issueNum
     * @param state
     * @return
     */
    List<BettingOrder> findByGameCodeAndIssueNumAndState(String gameCode, Long issueNum, String state);

    /**
     * @param bettingTime
     * @param state
     * @return
     */
    List<BettingOrder> findTop50ByBettingTimeGreaterThanAndStateOrderByTotalWinningAmountDesc(Date bettingTime,
                                                                                              String state);

    /**
     * @param state
     * @return
     */
    List<BettingOrder> findTop50ByStateOrderByTotalWinningAmountDesc(String state);

    /**
     * @param gameCode
     * @param issueNum
     * @return
     */
    List<BettingOrder> findByGameCodeAndIssueNum(String gameCode, Long issueNum);

}
