package me.zohar.lottery.betting.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author sunflower
 */
@Data
public class ChangeOrderParam {

	/**
	 * 投注订单id
	 */
	@NotBlank
	private String bettingOrderId;

	/**
	 * 投注记录id
	 */
	@NotBlank
	private String bettingRecordId;

	/**
	 * 游戏玩法代码
	 */
	@NotBlank
	private String gamePlayCode;

	/**
	 * 所选号码
	 */
	@NotBlank
	private String selectedNo;

}
