package me.zohar.lottery.betting.repo;

import me.zohar.lottery.betting.domain.BettingRebate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author sunflower
 */
public interface BettingRebateRepo
        extends JpaRepository<BettingRebate, String>, JpaSpecificationExecutor<BettingRebate> {
    /**
     * @return
     */
    List<BettingRebate> findBySettlementTimeIsNull();

    /**
     * @param bettingOrderId
     * @return
     */
    List<BettingRebate> findByBettingOrderId(String bettingOrderId);

}
